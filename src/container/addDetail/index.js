import React, { useEffect, useState } from 'react';
import { Button, Form, Row, Col } from 'react-bootstrap';
import storage from "../../firebaseConfig.js"
import { getDownloadURL, ref, uploadBytesResumable } from "firebase/storage"
import { useLocation } from 'react-router-dom';
const AddDetail = () => {
    const [name, setName] = useState('')
    const [phone, setPhone] = useState('')
    const [isWhatsApp, setIsWhatsApp] = useState('')
    const [type, setType] = useState('')
    const [profilePicture, setProfilePicture] = useState('');

    const saveData = (e) => {
        e.preventDefault()
        if (!profilePicture) {
            alert("Please choose a file first!")
        }
        const storageRef = ref(storage, `/test/${profilePicture[0].name}`)
        const uploadTask = uploadBytesResumable(storageRef, profilePicture);
        console.log(uploadTask, " uploadTask uploadTask uploadTask")
        uploadTask.on(
            "state_changed",
            (snapshot) => {
                const percent = Math.round(
                    (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                );

            },
            (err) => console.log(err, " jjjjj"),
            () => {
                // download url
                getDownloadURL(uploadTask.snapshot.ref).then((url) => {
                    console.log(url, "url url url ");
                    setProfilePicture(url)
                });
            }
        );
        var retrievedObject = localStorage.getItem('testObject');
        // localStorage.setItem('testObject', JSON.stringify([{ name: name, phone: phone, type: type, isWhatsApp: isWhatsApp, profilePicture: profilePicture }]));

        console.log(retrievedObject, "sjdhksahdkhskadk");
        if (retrievedObject !== null) {
            console.log("11111111");
            var users = JSON.parse(retrievedObject)
            users.push({ name: name, phone: phone, type: type, isWhatsApp: isWhatsApp, profilePicture: profilePicture })
            localStorage.setItem('testObject', JSON.stringify(users));
        } else {
            console.log("2222222222222");

            localStorage.setItem('testObject', JSON.stringify([{ name: name, phone: phone, type: type, isWhatsApp: isWhatsApp, profilePicture: profilePicture }]));
        }
        window.location.assign('/')

    }

    const search = useLocation().search;
    const id = new URLSearchParams(search).get('id');

    useEffect(() => {
        if (id) {
            var retrievedObject = localStorage.getItem('testObject');
            const users = JSON.parse(retrievedObject)
            console.log(users, "kkkkkkkkkkkkk", users[id]?.name);
            setName(users[id]?.name)
            setPhone(users[id]?.phone)
            setIsWhatsApp(users[id]?.isWhatsApp)
            setType(users[id]?.type)
        }
    }, [id])

    const updateDetail = (id) => {
        var retrievedObject = localStorage.getItem('testObject');
        const users = JSON.parse(retrievedObject)
        users[id].name = name;
        users[id].phone = phone;
        users[id].isWhatsApp = isWhatsApp;
        users[id].type = type
        console.log(id, "console", name, phone, isWhatsApp, type, "hjhjh", users);
        localStorage.setItem('testObject', JSON.stringify(users))
        window.location.assign('/')
    }
    return (
        <Row>
            <Col md={4}></Col>
            <Col md={4}>
                <div className="mainDiv p-4 m-5">
                    <h5 className="heading">Register For Free!</h5>
                    <p className='text-muted text p-4'>Create an account to save your data profile picture and upload it in any website</p>
                    <hr />
                    <Form>
                        <Form.Group className="mb-3 d-flex" controlId="formBasicEmail">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" className="inputType nameType" value={name} placeholder="Enter your name" onChange={(e) => setName(e.target.value)} />
                        </Form.Group>
                        <Form.Group className="mb-3 d-flex" controlId="formBasicEmail">
                            <Form.Label>Phone</Form.Label>
                            <Form.Control type="text" className="inputType phoneType" value={phone} placeholder="Enter your phone number" onChange={(e) => setPhone(e.target.value)} />
                        </Form.Group>
                        <Form.Group className="mb-3 d-flex" controlId="formBasicEmail">
                            <Form.Label>Type</Form.Label>
                            <Form.Select aria-label="Is WhatsApp" value={type} className="inputType typeInputType" onChange={(e) => setType(e.target.value)}>
                                <option>Select</option>
                                <option value="Office">Office</option>
                                <option value="Personnel">Personnel</option>
                            </Form.Select>
                        </Form.Group>
                        <Form.Group className="mb-3 d-flex" controlId="formBasicEmail">
                            <Form.Label>Is WhatsApp</Form.Label>
                            <Form.Select aria-label="Is WhatsApp" value={isWhatsApp} className="inputType" onChange={(e) => setIsWhatsApp(e.target.value)}>
                                <option>Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </Form.Select>
                        </Form.Group>
                        <Form.Group className="mb-3 d-flex" controlId="formBasicEmail">
                            <Form.Label>Profile Picture</Form.Label>
                            <Form.Control type="file" accept='image/*' className="inputType ms-5" onChange={(e) => setProfilePicture(e.target.files)} />
                        </Form.Group>
                        <Button onClick={(e) =>{id? updateDetail(id) :saveData(e)} } className="px-3 py-2" variant="outline-secondary">{id? 'Update':'Register'}</Button>
                    </Form>
                </div>
            </Col>
        </Row>
    )
}

export default AddDetail