import React, { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';


const ContactList = () => {
    var retrievedObject = localStorage.getItem('testObject');
    const users = JSON.parse(retrievedObject)
    const handleDelete = (index) => {
        users.splice(index, 1)
        console.log(users, "user deleted");
    }
    return (
        <>
            <h4>Contact List</h4>
            {
                users?.length > 0 ?
                    <Table striped bordered hover>
                        <thead>
                            <th>Sr. No.</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Type</th>
                            <th>isWhatsApp</th>
                            <th colSpan={2}>Action</th>
                        </thead>
                        <tbody>
                            {users?.map((user, i) => {
                                return (
                                    <tr>
                                        <td>{i + 1}</td>
                                        <td>{user.name}</td>
                                        <td>{user.phone}</td>
                                        <td>{user.type}</td>
                                        <td>{user.isWhatsApp}</td>
                                        <td role="button">
                                            <Link to={"/add-contact?id=" + i}>Edit</Link>
                                        </td>
                                        <td role="button" onClick={() => handleDelete(i)}>Delete</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </Table> :
                    <>
                        <p className="text-danger">No contact is available</p>
                        <a href="/add-contact">Add Contact</a>
                    </>
            }
        </>
    )
}

export default ContactList