import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import ContactList from './container/list';
import AddDetail from './container/addDetail';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          {/* list */}
          <Route path="/" element={<ContactList />} /> 
          {/* add detail */}
          <Route path="/add-contact" element={<AddDetail />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
